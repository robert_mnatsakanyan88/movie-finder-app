/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import GeneralStack from './src/navigation'
import { SafeAreaProvider } from 'react-native-safe-area-context';
const App  = () => {
  return (
    <SafeAreaProvider>
      <NavigationContainer>        
        <GeneralStack/>
      </NavigationContainer>
    </SafeAreaProvider>

  );
};

export default App;
