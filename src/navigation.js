import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Content from './Content';
import MovieInfo from './screens/movie_info';

const Stack = createNativeStackNavigator();

export default GeneralStack = () => {
	return (
		<Stack.Navigator>
		  <Stack.Screen
        name="Content"
        component={Content}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MovieInfo"
        component={MovieInfo}
        options={{headerShown: false}}
      />
		</Stack.Navigator>
	)
}