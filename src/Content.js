import React from 'react';
import AppHeader from './components/header'
import Searchbar from './components/searchbar/';
import Favorites from './components/favorites';

export default Content = () => {
	return (
		<>
		<AppHeader/>
		<Searchbar/>
		<Favorites/>	
		</>
	)
}