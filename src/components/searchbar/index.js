import React, { useEffect, useState } from 'react';
import {
	Alert,
	FlatList,
	TextInput,
	View,
	TouchableOpacity,
	Text,
	Image,
} from 'react-native';
import style from './style'
import {useFocusEffect, useNavigation } from '@react-navigation/native';
import AsyncStorage  from '@react-native-async-storage/async-storage';


export default SearchBar = () =>  {
	const [searchedData, setSearchedData] = useState([]);
	const [movieTitle, setMovieTitle] = useState(false);
 	const navigation = useNavigation();
	
	const searchMovie = async (movieTitle) => {
 		if(movieTitle.length){
			try {
				const res = await fetch(`https://imdb8.p.rapidapi.com/auto-complete?q=${movieTitle}`, {
					"method": "GET",
					"headers": {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						'x-rapidapi-host': 'imdb8.p.rapidapi.com',
    				'x-rapidapi-key': '8c07dfa36cmsh59887ba8fa9ce88p1a170fjsn765fd81ab2aa'
					}
					})					
					if(res.status === 200){
						const data = await res.json()
						filterSearchedData(data)				
					}else{
						Alert.alert(
							'Service Unavailable',
							'Connection Problem',
							[
								{ text: "OK", onPress: () => 	setSearchedData([]) }
							])
					}
			} catch (error) {
					Alert.alert('Connection problem', 'Please try again later')
			}
		}else{
			setSearchedData([])
		}		
	}

	const filterSearchedData = async (data) => {
		const hiddenMovies = JSON.parse( await AsyncStorage.getItem('hiddenMovies') )
		const searchedData = data.d
		if(hiddenMovies?.length){
			const filteredData =  filterSearchedAndHiddenMovies(searchedData,hiddenMovies);
			setSearchedData(filteredData)
		}else{
			setSearchedData(searchedData)
		}
	}

	const filterSearchedAndHiddenMovies = (searchedData, hiddenMovies) => {
		return searchedData.filter(searchedData => {
		  return !hiddenMovies.some(hiddenMovies => {
			return searchedData.id === hiddenMovies.id;
		  });
		});
	  }

	const Item = ({ item }) => (
		<TouchableOpacity onPress={() => navigation.navigate('MovieInfo', {movie : item})} style={style.searchedItemStyle}>
			<Image
				style={style.moviePoster}
				 source={{uri: item.i?.imageUrl}}/>
			<Text style={style.movieTitle}  >{item.l}</Text>
		</TouchableOpacity>
 	);

	const renderItem = ({ item }) => {
		return (
			<Item
				item ={item}				
			/>
		)
  };
return <View style={style.searchBarBox}>
		<View style={style.searchBar}> 
			<TextInput
				placeholder='Search for a movie'
				onSubmitEditing={()=> searchMovie(movieTitle)}
				onChangeText={movieTitle => setMovieTitle(movieTitle)}
				blurOnSubmit={true}
				style={{flex: 1}}
			/>
		
		</View> 
			<FlatList
			horizontal
			data={searchedData}
			renderItem={renderItem}
			keyExtractor={(item) => item.id}
			style={style.searchResultBox}
		/>
	</View>
  ;
}
	

 