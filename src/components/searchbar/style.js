import { StyleSheet } from "react-native";

export default StyleSheet.create({
	searchBarBox: {
		width: '90%',
		alignSelf: 'center'
	},
	searchBar: {
		borderWidth: 1,
		width: '90%',
		alignSelf: 'center',
		borderRadius: 10,
		borderColor: '#9da1a8',
		marginTop: '3%',
		display: 'flex',
		flexDirection: 'row'
	},
	searchedItemStyle: {
		alignItems: 'center',
		marginLeft: 10,
		marginRight: 10
	},
	movieTitle: {
		width: '85%',
		textAlign: 'center',
	},
	moviePoster: {
		width: 100,
		height: 100
	},
	searchResultBox:{
		marginTop: 10,
		width: '90%',
		alignSelf: 'center',
	}
})