import React from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';
import style from './style'

export default AppHeader = () => {
  return (
    <View style={style.sectionContainer}>
      <Text
        style={[
          style.sectionTitle,
        ]}>
        Welcome to movie finder
      </Text>      
    </View>
  );
};  