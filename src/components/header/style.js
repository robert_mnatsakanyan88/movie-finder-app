import { StyleSheet } from "react-native";

export default  StyleSheet.create({
    sectionContainer: {
      marginTop: 10,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
      textAlign: 'center',
      textTransform: 'capitalize'
    },
});