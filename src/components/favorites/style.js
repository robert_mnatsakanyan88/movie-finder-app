import { StyleSheet } from "react-native";

export default  StyleSheet.create({
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
      flex: 1
    },
    sectionTitle: {
      fontSize: 18,
      fontWeight: '600',
      textAlign: 'center',
      textTransform: 'capitalize',
      marginBottom: 5
    },
    favoriteItemStyle: {
      display: 'flex',
      flexDirection:'row',
      margin: 5
    },
    moviePoster: {
      width: 50,
      height:50
    },
    movieTitle: {
      textAlign: 'center',
    },
    rightPart: {
      display: 'flex',
      flexDirection: 'column',
      flex: 1
    }
});