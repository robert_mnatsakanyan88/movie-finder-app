import React, {useState, useEffect } from 'react';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image
} from 'react-native';
import style from './style'
import AsyncStorage  from '@react-native-async-storage/async-storage';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

export default Favorites = () => {
	const [favoriteMovies, setFavoriteMovies] = useState([])
	
	useFocusEffect(  
    React.useCallback(()=>{
      getFavoriteMoviesFromStorage()
    }, [])
	 )

	const getFavoriteMoviesFromStorage = async () => {
		const result = await AsyncStorage.getItem('favoriteMovies');
    if(result && result?.length){
      setFavoriteMovies(JSON.parse(result))
    }else{
      await AsyncStorage.setItem('favoriteMovies', JSON.stringify({}))
    }
	}

  const Item = ({ item }) => {
    const favoriteMovie = favoriteMovies[item] ;
    const navigation = useNavigation();
    return(
      <TouchableOpacity onPress={() => navigation.navigate('MovieInfo', {movie : favoriteMovie})} style={style.favoriteItemStyle}>
        <Image
          style={style.moviePoster}
          source={{uri: favoriteMovie.i?.imageUrl}}
        />
        <View style={style.rightPart}>
          <Text style={{...style.movieTitle, fontWeight: '700'}}>{favoriteMovie.l}</Text>
          <Text style={style.movieTitle}>{favoriteMovie.s}</Text>
        </View>
      </TouchableOpacity>
    )
 	};

	const renderItem = ({ item }) => {
		return (
			<Item
				item ={item}				
			/>
		)
  }
  return (
    <View style={style.sectionContainer}>
      <Text
        style={[
          style.sectionTitle,
        ]}>
        Your favorite movies list
      </Text>
        <FlatList
        data={Object.keys(favoriteMovies)}
        renderItem={renderItem}
        keyExtractor={(item) => favoriteMovies[item].id }        
      />   
     </View>
  );
};