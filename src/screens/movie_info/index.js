import React, { useEffect, useState } from 'react';
import { Text, TouchableOpacity, View, Image } from 'react-native';
import AsyncStorage  from '@react-native-async-storage/async-storage';
import style from './style'


export default MovieInfo = ({navigation, route}) => {
	const [isFavoriteMovie, setIsFavoriteMovie] = useState(false)
	const movie =route.params.movie;
	useEffect(()=> {
		isFavorite()
	})

	const isFavorite = async () => {
		const favoriteMovies = await AsyncStorage.getItem('favoriteMovies');
		const favoriteMoviesObject = JSON.parse(favoriteMovies);
		movie.id in favoriteMoviesObject && setIsFavoriteMovie(true) ;
	}

	const addToFavorites = async (movie) => {
		const favoriteMovies = await AsyncStorage.getItem('favoriteMovies');
		const favoriteMoviesObject = JSON.parse(favoriteMovies);
		favoriteMoviesObject[movie.id] = movie
		AsyncStorage.setItem('favoriteMovies', JSON.stringify(favoriteMoviesObject));
		setIsFavoriteMovie(true)
	}

	const removeFromFavorites = async (movie) => {
		const favoriteMovies = await AsyncStorage.getItem('favoriteMovies');
		const favoriteMoviesObject = JSON.parse(favoriteMovies);
		delete favoriteMoviesObject[movie.id]
		AsyncStorage.setItem('favoriteMovies', JSON.stringify(favoriteMoviesObject));
		setIsFavoriteMovie(false)
	}

	const hideMovie = async (movie) => {
		const hiddenMoviesFromStorage = await AsyncStorage.getItem('hiddenMovies');
		const hiddenMovies = JSON.parse(hiddenMoviesFromStorage)
		if(hiddenMovies){
			hiddenMovies.push(movie)		 
			await AsyncStorage.setItem('hiddenMovies', JSON.stringify(hiddenMovies));		 
		}else{					 
			await AsyncStorage.setItem('hiddenMovies', JSON.stringify([movie]));		 
		}
		
		navigation.navigate('Content')
 	}
	return (
		<View style ={style.mainWrapper}> 
			<TouchableOpacity onPress={()=>navigation.navigate('Content')}>
				<Text style={style.goBackButton}> Go Back </Text>
			</TouchableOpacity>
			<View style={style.movieInfoBody}>
				<View style={style.movieInfoTop}>
					<Image style={style.moviePoster} source={ {uri: movie.i.imageUrl}}/>
					<View style={style.movieTitleBox}>
						<Text style={{fontSize: 20, fontWeight: '700'}}>{movie.l}</Text>
						<Text>{movie.s}</Text>
						<Text>Rank - {movie.rank}</Text>
					</View>
				</View>
				<View style={style.movieInfoBottom}>
 					{
						!isFavoriteMovie
					?
					<>
					<TouchableOpacity style={style.addToFavoritesButton} onPress={() => addToFavorites(movie)}>
						<Text style={{color: 'white'}}>Add to favorites</Text>
					</TouchableOpacity>
					<TouchableOpacity style={style.hideMovieButton} onPress={() => hideMovie(movie)}>
						<Text style={{color: 'white'}}>Hide movie</Text>
					</TouchableOpacity>
					</>
					:
					<TouchableOpacity style={style.removeFromFavoritesButton} onPress={() => removeFromFavorites(movie)}>
						<Text style={{color: 'white'}}>Remove from favorites</Text>
					</TouchableOpacity>
					}
				</View>
			</View>
		</View>
	)
}