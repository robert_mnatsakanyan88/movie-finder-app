import { StyleSheet } from "react-native";

export default StyleSheet.create({
	mainWrapper: {
		flex: 1,
	},
	goBackButton : {
		color: 'blue',
		fontSize: 15,
	},
	movieInfoBody: {
		flex: 1,
		display: 'flex',
		flexDirection: 'column',
		marginTop: 20,
		width: '95%',
		alignSelf: 'center',
 	},
	moviePoster: {
		width: 150,
		height: 200
	},
	movieInfoTop: {
		display: 'flex',
		flexDirection: 'row',
	},
	movieTitleBox: {
		flex: 1,
 		alignItems: 'center',
		justifyContent: 'center'
	},
	movieInfoBottom: {
 		display: 'flex',
		flexDirection: 'row',
		marginTop : '10%',
		justifyContent: 'flex-end'
 	},
	addToFavoritesButton: {
 		padding: 10,
		borderRadius: 10,
		backgroundColor: '#408763',
	},
	hideMovieButton: {
		padding: 10,
		borderRadius: 10,
		backgroundColor: '#888c89',
   },
	removeFromFavoritesButton: {
 		padding: 10,
		borderRadius: 10,
		backgroundColor: '#f2867e'
	}
})